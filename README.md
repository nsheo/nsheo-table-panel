# NSHEO Table Panel
### Getting Started

1. Make a subdirectory named after your plugin in the `data/plugins` subdirectory in your Grafana instance. It does not really matter what the directory name is. When the plugin is installed via the grafana cli, it will create a directory named after the plugin id field in the plugin.json file.

1. Copy the files in this project into your new plugin subdirectory.
2. `npm install` or `yarn install`
3. `grunt`
4. `karma start --single-run` to run the tests once. There is one failing test for the `testDatasource` in the datasource.ts file.
5. Restart your Grafana server to start using the plugin in Grafana (Grafana only needs to be restarted once).

`grunt watch` will build the TypeScript files and copy everything to the dist directory automatically when a file changes. This is useful for when working on the code. `karma start` will turn on the karma file watcher so that it reruns all the tests automatically when a file changes.

Changes should be made in the `src` directory. The build task transpiles the TypeScript code into JavaScript and copies it to the `dist` directory. Grafana will load the JavaScript from the `dist` directory and ignore the `src` directory.

### Run to Test

1. Source 다운로드   
  `git`을 사용해 플러그인을 다운 받는다. 

  ```bash
  git clone https://gitlab.com/nsheo/nsheo-table-panel.git
  ```
2. Build   
  `yarn` 혹은 `npm`을 통해 프로젝트를 실행하고 `grunt`를 사용해 build 한다

  1. npm 설치 

    ```bash
    yum install -y gcc-c++ make 	#의존성 패키지 설치   
    curl -sL https://rpm.nodesource.com/setup_10.x | sudo -E bash - # nodejs 최신버전 저장소 설치   
    yum install nodejs npm # nodejs 및 npm 설치
    ```

  2. yarn 설치

    curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo #저장소 설치   
    rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg #인증키 추가   
    yum install yarn

  3. grunt설치

    ```bash
    npm install -g grunt #전역변수로 사용가능하게끔 설치   
    ```

    

  4. 프로젝트 빌드

    ```bash
    cd nsheo-table-panel   
    yarn install / npm install   
    grunt
    ```


3. docker 배포   

  grunt 이후 dist 폴더의 내용을 /var/lib/grafana/plugins/nsheo-table-panel 로 이동해도 사용가능   
  docker에 플러그인 하나만 설치해서 사용할 경우엔 아래 커맨드로 사용가능 

  ```bash
  docker run --net=host -d --name grafana-plugin-dev --volume $(pwd)/dist:/var/lib/grafana/plugins/nsheo-table-panel    grafana/grafana  
  
  #docker 삭제 및 이미지 제거 
  docker stop grafana-plugin-dev   
  docker rm grafana-plugin-dev   
  ```



### 관련 도구 설명   

1. npm : node package manager / yarn   
gradle과 같이 nodejs에서 사용할 수 있는 모듈들을 패키지화하여 모아둔 저장소 및 설치 및 관리를 위한 cli 제공   

2. grunt   
사전정의된 task 단위로 실행되며 command line을 통해 동작하는 자바스크립트 용 빌드 `자동화`툴   
`Grunt.js`를 이용하여 검증 및 빌드에 대한 순서를 사전정의하여 사용   

### CHANGELOG

#### v0.0.1
- First version.